import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpAuthInterceptor } from './service/http.auth.interceptor';
import { LoggerComponent } from './component/logger/logger.component';
import { SingleLoggerComponent } from './component/logger/single-logger/single-logger.component';
import { KeysPipe } from './pipe/keys.pipe';
import { HttpTraceComponent } from './component/http-trace/http-trace.component';
import { SingleTraceComponent } from './component/http-trace/single-trace/single-trace.component';

@NgModule({
  declarations: [
    AppComponent,
    LoggerComponent,
    SingleLoggerComponent,
    KeysPipe,
    HttpTraceComponent,
    SingleTraceComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpAuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
