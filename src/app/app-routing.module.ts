import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { LoggerComponent } from './component/logger/logger.component';
import { HttpTraceComponent } from './component/http-trace/http-trace.component';

const routes: Routes = [
  { path: '', component: AppComponent },
  { path: 'loggers', component: LoggerComponent },
  { path: 'traces', component: HttpTraceComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
