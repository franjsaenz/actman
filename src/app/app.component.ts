import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthenticationService } from './service/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  collapsed: boolean = true;

  authForm: FormGroup;

  isLogin: boolean;

  constructor (
    private authenticationService: AuthenticationService
  ) {}

  ngOnInit() {
    this.isLogin = this.authenticationService.isLoggedin();
    this.authForm = new FormGroup({
      token: new FormControl('')
    })
  }

  login() {
    this.authenticationService.login(this.authForm.get('token').value);
    this.isLogin = true;
    this.authForm.get('token').setValue('');
  }

  logout() {
    this.authenticationService.logout();
    this.isLogin = false;
  }

}
