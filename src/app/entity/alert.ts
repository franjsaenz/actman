export class Alert {
    type: string;
    message: string;
    dismissible: boolean;
}