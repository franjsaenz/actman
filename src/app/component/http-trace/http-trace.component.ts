import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { debounceTime } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-http-trace',
  templateUrl: './http-trace.component.html'
})
export class HttpTraceComponent implements OnInit {

  baseUrl: string = environment.baseUrl + '/actuator/httptrace';

  traces: Array<any> = [];

  isLoading = true;

  isError = false;

  searchInput: FormControl;

  searchTerm: string;

  constructor(
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.searchInput = new FormControl();
    this.searchInput.valueChanges
      .pipe(debounceTime(200))
      .subscribe(term => {
        if (term == null || term == '') {
          this.searchTerm = null;
        } else {
          this.searchTerm = term;
        }
      });
    this.getTraces();
  }

  getTraces() {
    this.traces = [];
    this.isLoading = true;
    this.http.get<any>(this.baseUrl).subscribe(res => {
      this.isLoading = false;
      this.isError = false;
      this.traces = res.traces;
    }, err => {
      this.isLoading = false;
      this.isError = true;
      console.log(err);
    })
  }

  clear() {
    this.searchTerm = null;
  }

  filter(trace: any, term: string) {
    if (trace == null || trace.request == null) {
      return false;
    }
    if (term == null) {
      return true;
    }
    let request: any = trace.request;
    let mtd: boolean = request.method.toLowerCase().indexOf(term.toLowerCase()) >= 0;
    let uri: boolean = request.uri.toLowerCase().indexOf(term.toLowerCase()) >= 0;
    return mtd || uri;
  }

}
