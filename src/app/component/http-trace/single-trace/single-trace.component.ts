import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-single-trace',
  templateUrl: './single-trace.component.html'
})
export class SingleTraceComponent implements OnInit {

  @Input() trace: any;

  constructor() { }

  ngOnInit() {
  }

}
