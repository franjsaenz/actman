import { Injectable } from "@angular/core";
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from "./authentication.service";

@Injectable()
export class HttpAuthInterceptor implements HttpInterceptor {

    constructor(
        private authenticationSerive: AuthenticationService
    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let h = req.headers;
        h = h.set('Content-Type', 'application/json');
        const token: string = this.authenticationSerive.getToken();
        if (token != null) {
            h = h.set('Authorization', 'Bearer ' + token);
        }
        const clonedRequest = req.clone({ headers: h });
        return next.handle(clonedRequest);
    }

}