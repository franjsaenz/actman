import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { debounceTime } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-logger',
  templateUrl: './logger.component.html'
})
export class LoggerComponent implements OnInit {

  baseUrl: string = environment.baseUrl + '/actuator/loggers';

  loggers: any = {};

  isLoading = true;

  isError = false;

  searchInput: FormControl;

  searchTerm: string;

  constructor(
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.searchInput = new FormControl();
    this.searchInput.valueChanges
      .pipe(debounceTime(200))
      .subscribe(term => {
        if (term == null || term == '') {
          this.searchTerm = null;
        } else {
          this.searchTerm = term;
        }
      });
    this.getLoggers();
  }

  getLoggers() {
    this.loggers = {};
    this.isLoading = true;
    this.http.get<any>(this.baseUrl).subscribe(res => {
      this.isLoading = false;
      this.isError = false;
      this.loggers = res.loggers;
    }, err => {
      this.isLoading = false;
      this.isError = true;
      console.log(err);
    })
  }

  clear() {
    this.searchTerm = null;
  }

  filter(logger: any, term: string) {
    if (logger == null || logger.key == null) {
      return false;
    }
    if (term == null) {
      return true;
    }
    let key: string = logger.key;
    return key.toLowerCase().indexOf(term.toLowerCase()) >= 0;
  }

}
