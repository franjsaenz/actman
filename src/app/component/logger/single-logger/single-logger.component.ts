import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-single-logger',
  templateUrl: './single-logger.component.html'
})
export class SingleLoggerComponent implements OnInit {

  baseUrl: string = environment.baseUrl + '/actuator/loggers';
  
  @Input() logger: any;

  constructor(
    private http: HttpClient
  ) { }

  ngOnInit() {
  }

  changeLevel(level: string) {
    this.http.post(this.baseUrl + '/' + this.logger.key, {configuredLevel: level}).subscribe(res => {
      this.logger.value.effectiveLevel = level;
    }, err => {
      console.log(err);
    })
  }

}
