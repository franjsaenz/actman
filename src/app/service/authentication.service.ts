import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private tokenField: string = 'actman-token';

  constructor() { }

  public isLoggedin(): boolean {
    return this.getToken() != null;
  }

  public login(token: string) {
    localStorage.setItem(this.tokenField, token);
  }

  public logout() {
    localStorage.removeItem(this.tokenField);
  }

  public getToken() {
    return localStorage.getItem(this.tokenField)
  }

}
